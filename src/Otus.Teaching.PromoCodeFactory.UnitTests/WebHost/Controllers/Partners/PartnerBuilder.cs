﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private Partner _partner = new(); //public PartnerBuilder() => _partner = new Partner();

        public PartnerBuilder Name(string name)
        {
            _partner.Name = name;   
            return this;
        }
        public PartnerBuilder NumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _partner.NumberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public PartnerBuilder IsActive
        {
            get {
                _partner.IsActive = true;
                return this;
            }           
        }
        public PartnerBuilder IsNotActive
        {
            get
            {
                _partner.IsActive = false;
                return this;
            }
        }

        public Partner Build()
        {
            return _partner;
        }
    }
}
