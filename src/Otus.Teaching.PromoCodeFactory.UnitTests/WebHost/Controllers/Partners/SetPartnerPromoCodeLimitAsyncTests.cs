using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestsFixtureInit>//, IDisposable
    {
        private readonly TestsFixtureInit _fixture;
        public SetPartnerPromoCodeLimitAsyncTests(TestsFixtureInit fixtureInit)
        {
            _fixture = fixtureInit;
        }
        //TODO: Add Unit Tests

        [Fact(DisplayName = "1. Если партнер не найден, то также нужно выдать ошибку 404;")]     //[AutoMoqData]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerNotFound_Return404()
        {
            //Arrange
            var partnerId = Guid.NewGuid();//Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _fixture.PartnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                                           .ReturnsAsync(partner);

            //Act
            var result = await _fixture.PartnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId, _fixture.setPartnerPromoCodeLimitRequest);

            //Assert
            Assert.IsType<NotFoundResult>(result);
            Assert.True(result is NotFoundResult);
            Assert.Equal(404, (result as StatusCodeResult)?.StatusCode);
            //fluentAssertion
            result.Should().BeOfType<NotFoundResult>();
            (result as StatusCodeResult)?.StatusCode.Should().Be(404);
        }

        [Fact(DisplayName = "2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400;")]
        public async void SetPartnerPromoCodeLimitAsync_IfPartnerNotActive_Return400()
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                         .IsNotActive
                         .Build(); //new Partner() { IsActive = false };       

            _fixture.PartnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                                   .ReturnsAsync(partner);
            //Act
            var result = await _fixture.PartnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId,
                                                                                         _fixture.setPartnerPromoCodeLimitRequest);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result is BadRequestObjectResult);
            Assert.Equal(400, (result as BadRequestObjectResult)?.StatusCode);
            Assert.Equal("Данный партнер не активен", (result as BadRequestObjectResult).Value);
            //fluentAssertion
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact(DisplayName = "3. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;")]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitGreaterZero_SetNumberIssuedPromoCodesToZero()
        {
            //Arrange            
            var partnerController = new PartnersController(_fixture.PartnersRepositoryInMem);

            //партнер с неизрасходованным лимитом промокодов
            var partner = (await _fixture.PartnersRepositoryInMem.GetAllAsync())
                          .FirstOrDefault(p => p.NumberIssuedPromoCodes <
                                           p.PartnerLimits.FirstOrDefault(l => l.Limit > 1 && !l.CancelDate.HasValue)?.Limit
                                         );
            //установить кол-во промокодов > 0
            if (partner.NumberIssuedPromoCodes == 0)
            {
                partner.NumberIssuedPromoCodes = 1;
                await _fixture.PartnersRepositoryInMem.UpdateAsync(partner);
            }

            var limit = new SetPartnerPromoCodeLimitRequestBuilder()
                .EndDate(DateTime.Now)
                .Limit(123456)
                .Build();

            //Act
            await partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, limit);

            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact(DisplayName = "4. При установке лимита нужно отключить предыдущий лимит;")]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitGreaterZero_TurnOffExistingLimit()
        {
            //Arrange            
            var partnerController = new PartnersController(_fixture.PartnersRepositoryInMem);

            //активный партнер 
            var partner = (await _fixture.PartnersRepositoryInMem.GetAllAsync())
                          .FirstOrDefault(p => p.IsActive);

            var prevLimit = partner.PartnerLimits.FirstOrDefault(l => !l.CancelDate.HasValue);

            //Act
            await partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, _fixture.setPartnerPromoCodeLimitRequest);

            //Assert
            prevLimit.CancelDate.Should().HaveValue();
        }


        [Theory(DisplayName = "5. Лимит должен быть больше 0;")]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitLessOrEqualZero_Return400(int limit)
        {
            //Arrange
            var partnerId = Guid.NewGuid();
            var partner = new PartnerBuilder()
                         .IsActive
                         .Build(); //new Partner() { IsActive = false };            

            _fixture.PartnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                                           .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequest() { Limit = limit };

            //Act
            var result = await _fixture.PartnersControllerMock.SetPartnerPromoCodeLimitAsync(partnerId,
                                                                                             request);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result is BadRequestObjectResult);
            Assert.Equal(400, (result as BadRequestObjectResult)?.StatusCode);
            Assert.Equal("Лимит должен быть больше 0", (result as BadRequestObjectResult).Value);
            //fluentAssertion
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact(DisplayName = "6. Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);")]
        public async void SetPartnerPromoCodeLimitAsync_IfLimitGreaterZero_ShouldSaveLimitToDb()
        {
            //Arrange            
            var partnerController = new PartnersController(_fixture.PartnersRepositoryInMem);

            //активный партнер 
            var partner = (await _fixture.PartnersRepositoryInMem.GetAllAsync())
                          .FirstOrDefault(p => p.IsActive);

            //Act
            var result = await partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, _fixture.setPartnerPromoCodeLimitRequest);
            //prevLimit = (await _fixture.PartnersRepository.GetByIdAsync(partner.Id)).PartnerLimits.FirstOrDefault(l=>l.Id == prevLimit.Id);           

            //Assert
            (result as CreatedAtActionResult).RouteValues.FirstOrDefault(p => p.Key == "limitId").Value
                .Should().BeOfType<Guid>().And
                .NotBeNull(); 
        }
    }
}