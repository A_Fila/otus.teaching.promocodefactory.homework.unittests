﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitRequestBuilder
    {
        SetPartnerPromoCodeLimitRequest _request = new ();

        public SetPartnerPromoCodeLimitRequestBuilder Limit(int limit)
        {
            _request.Limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequestBuilder EndDate(DateTime endDate)
        {
            _request.EndDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build() 
        {
            return _request;
        }
    }
}
