﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using FluentAssertions.Common;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.WebHost;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
//using System.Configuration;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class TestsFixtureInit: IDisposable//: Startup
    {
        DataContext _context;
        readonly IFixture _fixture = new Fixture().Customize(new AutoMoqCustomization());

        public IRepository<Partner> PartnersRepositoryInMem { get; set; }
        public readonly Mock<IRepository<Partner>> PartnersRepositoryMock;// = new Mock<IRepository<Partner>>();
        public readonly PartnersController PartnersControllerMock;
        
        public readonly SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(p => p.Limit, 43).Create();

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestsFixtureInit()//IServiceCollection services)// : base(configuration)
        {
            PartnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            PartnersControllerMock = _fixture.Build<PartnersController>().OmitAutoProperties().Create();// _fixture.Create<PartnersController>();    

            //инициализация InMemoryDatabase
            var builder = new DbContextOptionsBuilder<DataContext>();
            builder.UseInMemoryDatabase("InMemDb");    //DbContextOptions<DataContext> options = builder.Options;
            _context = new DataContext(builder.Options);

            var dbInitializer = new EfDbInitializer(_context);
            dbInitializer.InitializeDb();

            PartnersRepositoryInMem = new EfRepository<Partner>(_context);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}